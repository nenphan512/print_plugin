import 'package:flutter/material.dart';
import 'package:print_plugin_example/text_widget.dart';

class PrintingView extends StatelessWidget {
  const PrintingView({Key? key, this.isPrinting = false}) : super(key: key);
  final bool isPrinting;
  @override
  Widget build(BuildContext context) {
    return Visibility(
        visible: isPrinting,
        child: Container(
          color: Colors.white.withOpacity(.9),
          child: const Center(
              child: EviText(
            'PRINTING',
            fontSize: 30,
          )),
        ));
  }
}
