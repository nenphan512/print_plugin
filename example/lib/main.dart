import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:print_plugin/print_plugin.dart';
import 'package:print_plugin_example/const.dart';
import 'package:print_plugin_example/printing_view.dart';
import 'package:print_plugin_example/text_widget.dart';
import 'package:screenshot/screenshot.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  final List<BleData> listBle = [];
  bool isPrinting = false;
  bool isConnected = false;
  String selectedDevice = '';
  String prevDevice = '';

  ScreenshotController screenshotController = ScreenshotController();

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  Future<void> initPlatformState() async {
    PrintPlugin.listenerMethod(() {
      setState(() {
        isPrinting = false;
      });
      Fluttertoast.showToast(msg: 'Print success');
    }, onPrintFail: () {
      setState(() {
        isPrinting = false;
      });
      Fluttertoast.showToast(msg: 'Print fail');
    }, onBleSuccess: (address) {
      isConnected = true;
      setState(() {});
    }, onBleFail: () {
      isConnected = false;
      setState(() {});
    });
    String platformVersion;
    try {
      platformVersion =
          await PrintPlugin.platformVersion ?? 'Unknown platform version';
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }
    await fetchBle(true);
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  Future fetchBle([bool isFirst = false]) async {
    log('Fetch data');
    final data = await PrintPlugin.getDataList;
    listBle.clear();
    listBle.addAll(
        (jsonDecode(data) as List).map((e) => BleData.formJson(e)).toList());
    setState(() {});

    if (isFirst) {
      if (listBle.isNotEmpty) {
        PrintPlugin.connectBle(listBle[0].address!, '');
        selectedDevice = listBle[0].address!;

        setState(() {});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Screenshot(
        controller: screenshotController,
        child: Scaffold(
          appBar: AppBar(
            title: const EviText(
              'Print plugin example app',
              color: Colors.white,
            ),
            elevation: 0,
            backgroundColor: blueDark,
          ),
          body: Stack(
            children: [
              SingleChildScrollView(
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            const EviText('Bluetooth Devices'),
                            const Spacer(),
                            Container(
                              width: 13,
                              height: 13,
                              decoration: BoxDecoration(
                                  color:
                                      isConnected ? Colors.green : Colors.red,
                                  shape: BoxShape.circle),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            EviText(
                                isConnected ? 'Connected' : 'Not connected'),
                            const SizedBox(
                              width: 20,
                            ),
                            IconButton(
                                onPressed: () {
                                  fetchBle();
                                },
                                icon: const Icon(Icons.replay))
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Container(
                          height: 300,
                          decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(.1),
                              borderRadius: BorderRadius.circular(15)),
                          padding: const EdgeInsets.all(10),
                          child: listBle.isEmpty
                              ? const Center(child: EviText('No device'))
                              : ListView.builder(
                                  itemCount: listBle.length,
                                  itemBuilder: (_, index) {
                                    return TextButton(
                                      onPressed: () async {
                                        prevDevice = selectedDevice;

                                        selectedDevice =
                                            listBle[index].address!;

                                        setState(() {});

                                        if (prevDevice != '') {
                                          await PrintPlugin.connectBle(
                                              selectedDevice, '1');
                                        } else {
                                          await PrintPlugin.connectBle(
                                              selectedDevice, '');
                                        }
                                      },
                                      child: Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 20, horizontal: 10),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15),
                                            color: Colors.white),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                            EviText(listBle[index].name!),
                                            const SizedBox(
                                              width: 20,
                                            ),
                                            EviText(listBle[index].address!),
                                            const SizedBox(
                                              width: 20,
                                            ),
                                            listBle[index].address ==
                                                    selectedDevice
                                                ? const Icon(
                                                    Icons.check,
                                                    size: 30,
                                                  )
                                                : const SizedBox(
                                                    width: 30,
                                                  )
                                          ],
                                        ),
                                      ),
                                    );
                                  }),
                        ),
                        const SizedBox(
                          height: 50,
                        ),
                        const EviText('Print Functions'),
                        const SizedBox(
                          height: 15,
                        ),
                        Wrap(
                          alignment: WrapAlignment.start,
                          runAlignment: WrapAlignment.start,
                          crossAxisAlignment: WrapCrossAlignment.start,
                          spacing: 20,
                          runSpacing: 10,
                          children: [
                            MainButton(
                              onTap: () async {
                                if (listBle.isNotEmpty) {
                                  setState(() {
                                    isPrinting = true;
                                  });
                                  File file = await getImageFileFromAssets(
                                      'capture.png');
                                  Uint8List data = file.readAsBytesSync();
                                  screenshotController.capture().then((value) {
                                    PrintPlugin.printImage(data);
                                  });
                                }
                              },
                              text: 'Print bill',
                            ),
                            MainButton(
                              onTap: () {
                                if (listBle.isNotEmpty) {
                                  setState(() {
                                    isPrinting = true;
                                  });
                                  PrintPlugin.printContent(
                                      'Print text example - テキストの例を印刷する');
                                }
                              },
                              text: 'Print EviText',
                            ),
                            MainButton(
                              onTap: () {
                                if (listBle.isNotEmpty) {
                                  setState(() {
                                    isPrinting = true;
                                  });
                                  PrintPlugin.printBarcode('Barcode example');
                                }
                              },
                              text: 'Print barcode',
                            ),
                            MainButton(
                              onTap: () {
                                setState(() {
                                  isPrinting = true;
                                });
                                if (listBle.isNotEmpty) PrintPlugin.printInfo();
                              },
                              text: 'Print printer info',
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              PrintingView(
                isPrinting: isPrinting,
              )
            ],
          ),
        ),
      ),
    );
  }
}

class BleData {
  const BleData(this.name, this.address);

  final String? name;
  final String? address;

  factory BleData.formJson(Map<String, dynamic> json) =>
      BleData(json['name'], json['address']);
}

class MainButton extends StatelessWidget {
  const MainButton({Key? key, this.onTap, this.text}) : super(key: key);
  final VoidCallback? onTap;
  final String? text;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      color: blueDark,
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      onPressed: () {
        onTap!();
      },
      child: EviText(
        text!,
        fontSize: 18,
        color: Colors.white,
      ),
    );
  }
}
