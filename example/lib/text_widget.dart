import 'package:flutter/material.dart';

class EviText extends StatelessWidget {
  const EviText(this.text,
      {Key? key, this.fontSize = 13, this.color = Colors.black})
      : super(key: key);

  final String text;
  final double fontSize;
  final Color color;
  @override
  Widget build(BuildContext context) {
    return Text(text, style: TextStyle(fontSize: fontSize, color: color));
  }
}
