package com.print_plugin

import android.app.Activity
import android.graphics.BitmapFactory
import androidx.annotation.NonNull
import com.google.gson.Gson
import com.print_plugin.print.DataBle
import com.print_plugin.print.PrintFunc
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import java.util.*
import kotlin.concurrent.timerTask


/** PrintPlugin */
class PrintPlugin: FlutterPlugin, MethodCallHandler, ActivityAware {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  companion object Instance{
    private lateinit var channel : MethodChannel

    @JvmStatic
    fun updatePrintStatus(isSuccess:Boolean, delay:Long){
      if(isSuccess) {
        channel.invokeMethod("printSuccess",delay)

      }else{
        channel.invokeMethod("printFail",delay)
      }
    }

    @JvmStatic
    fun updateBleStatus(isSuccess:Boolean, address:String){
      if(isSuccess) {
        channel.invokeMethod("bleSuccess",address)

      }else{
        channel.invokeMethod("bleFail",address)
      }
    }
  }
  private lateinit var activity: Activity
  private lateinit var printFunc : PrintFunc

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "print_plugin")
    channel.setMethodCallHandler(this)
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    when (call.method) {
      "getPlatformVersion" -> {
        result.success("Android ${android.os.Build.VERSION.RELEASE}")
      }
      "getListBle" -> {
        val list = PrintFunc().setBluetooth(activity)
        val gson = Gson()
        val json: String = gson.toJson(list)
        return result.success(json)
      }

      "connectBle" ->{
        val address = call.arguments as List<String>
        printFunc.connectBle(DataBle(address = address[0]),DataBle(address = address[1]),activity)
        return result.success(true)
      }

      "printImage" -> {
        val image = call.arguments as ByteArray
        val bitmap = BitmapFactory.decodeByteArray(image, 0, image.size)
        printFunc.printPicture(bitmap,activity)
        return result.success(true)
      }
      "printContent" -> {
      printFunc.printContent(activity, call.arguments as String)
      return result.success(true)
      }
      "printInfo" ->{
        printFunc.printInfo(activity)
        return result.success(true)
      }
      "printBarcode"->{
        printFunc.printBarcode(activity, call.arguments as String)
      }
      else -> {
        result.notImplemented()
      }
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
    printFunc.destroy(activity)
  }

  override fun onAttachedToActivity(binding: ActivityPluginBinding) {
    activity = binding.activity
    //bind service，get ImyBinder object
    printFunc = PrintFunc()
    printFunc.initFunc(activity)
  }

  override fun onDetachedFromActivityForConfigChanges() {

  }

  override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {

  }

  override fun onDetachedFromActivity() {

  }
}
