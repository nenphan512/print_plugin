package com.print_plugin.print;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.print_plugin.PrintPlugin;
import com.print_plugin.utils.Conts;

import net.posprinter.posprinterface.IMyBinder;
import net.posprinter.posprinterface.ProcessData;
import net.posprinter.posprinterface.UiExecute;
import net.posprinter.service.PosprinterService;
import net.posprinter.utils.BitmapToByteData;
import net.posprinter.utils.DataForSendToPrinterPos80;
import net.posprinter.utils.DataForSendToPrinterTSC;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class PrintFunc {

    public static IMyBinder binder;

    //bindService connection
    ServiceConnection conn= new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            //Bind successfully
            binder= (IMyBinder) iBinder;
            Log.e("binder","connected");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.e("disbinder","disconnected");
        }
    };

    public void initFunc(Activity activity){
        Intent intent=new Intent(activity, PosprinterService.class);
        activity.bindService(intent, conn, Context.BIND_AUTO_CREATE);
    }

    public void destroy(Activity activity){
        binder.disconnectCurrentPort(new UiExecute() {
            @Override
            public void onsucess() {

            }

            @Override
            public void onfailed() {

            }
        });
        activity.unbindService(conn);
    }

    private void showSnackbar(String showstring, Context context){
        Toast.makeText(context ,showstring, Toast.LENGTH_LONG).show();
    }

    public List<DataBle> setBluetooth(Activity activity){
        BluetoothAdapter bluetoothAdapter= BluetoothAdapter.getDefaultAdapter();

        if (!bluetoothAdapter.isEnabled()){
            //open bluetooth
            Intent intent=new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(intent, Conts.ENABLE_BLUETOOTH);
            return new ArrayList<DataBle>();
        }else {

            return  findAvalibleDevice(bluetoothAdapter);

        }
    }

    private List<DataBle> findAvalibleDevice(BluetoothAdapter bluetoothAdapter) {

        Set<BluetoothDevice> device=bluetoothAdapter.getBondedDevices();

        if(bluetoothAdapter.isDiscovering()){
            return new ArrayList<DataBle>();
        }
        if(device.size()>0){

            //already
//            deviceList_bonded.add(btd.getName()+'\n'+btd.getAddress());
//            DataBle

            List<DataBle> list = new ArrayList<DataBle>();
            for (BluetoothDevice btd : device) {
                list.add(new DataBle(btd.getName(), btd.getAddress()));
            }
            return list;
        }else{
           return new ArrayList<DataBle>();
        }

    }

    public void connectBle(final DataBle dataBle,final DataBle prevDataBle, final Context context){
        if (!prevDataBle.getAddress().equals("")){
            binder.disconnectCurrentPort( new UiExecute() {
                @Override
                public void onsucess() {}

                @Override
                public void onfailed() {}
            });
        }
        if (dataBle.getAddress().equals("")){
            showSnackbar("Can not get device address",context);
        }else {
            binder.connectBtPort(dataBle.getAddress(), new UiExecute() {
                @Override
                public void onsucess() {
//                    showSnackbar("Connected",context);
                    PrintPlugin.updateBleStatus(true, dataBle.getAddress());
                    binder.write(DataForSendToPrinterPos80.openOrCloseAutoReturnPrintState(0x1f), new UiExecute() {
                        @Override
                        public void onsucess() {
                            binder.acceptdatafromprinter(new UiExecute() {
                                @Override
                                public void onsucess() {
//                                    showSnackbar("Connected printer",context);

                                    PrintPlugin.updateBleStatus(true,dataBle.getAddress());
                                }
                                @Override
                                public void onfailed() {
//                                    showSnackbar("Disconnected printer", context);
                                    PrintPlugin.updateBleStatus(false,dataBle.getAddress());
                                }
                            });
                        }
                        @Override
                        public void onfailed() {
                            showSnackbar("Connect printer failed",context);
                            PrintPlugin.updateBleStatus(false,dataBle.getAddress());
                        }
                    });
                }
                @Override
                public void onfailed() {
//                    showSnackbar("Connect BT port failed",context);
                    PrintPlugin.updateBleStatus(false,dataBle.getAddress());
                }
            });
        }
    }

    public void printPicture(final Bitmap a, Context context) {
        Bitmap b;
        double width = Math.round(a.getWidth());
        double height = Math.round(a.getHeight());
        double buildInWidth = 750;
        double ratio =1;

        ratio = buildInWidth/width;

        width = (int) (width*ratio);
        height = (int) (height*ratio);


        b = Bitmap.createScaledBitmap(a,(int) width,(int) height, false);
        
        //2s to recieve data, 4s to print
        double mm = height/8;
        double cm = mm/10;
        double inch = cm*0.393701;
        long delay = Math.round(inch/2*1000);
//        a.recycle();
        if(width>0&&height>0){
            setupImageToPrint(b,context,(int) width,(int) height,(int) delay);
        }else{
            PrintPlugin.updatePrintStatus(false,0);
        }
    }

    void setupImageToPrint(final Bitmap b, final Context context, final int width, final int height,final int delay){
        if (b==null){showSnackbar("b is null", context);}else {
            binder.writeDataByYouself(new UiExecute() {
                @Override
                public void onsucess() {
                    ////Wait printer
//                    showSnackbar("print succeed",context);


                    PrintPlugin.updatePrintStatus(true,4000+delay);
                }
                @Override
                public void onfailed() {
//                    showSnackbar("print fail",context);

                    PrintPlugin.updatePrintStatus(false,0);
                }
            }, new ProcessData() {
                @Override
                public List<byte[]> processDataBeforeSend() {
                    ArrayList<byte[]> list=new ArrayList<byte[]>();

                    list.add(DataForSendToPrinterTSC.sizeBydot(width,height+200));
                    list.add(DataForSendToPrinterTSC.cls());
                    list.add(DataForSendToPrinterTSC.density(7));
                    list.add(DataForSendToPrinterTSC.speed(7));
                    list.add(DataForSendToPrinterTSC.direction(1));
                    list.add(DataForSendToPrinterTSC.reference(0,100));

                    list.add(DataForSendToPrinterTSC.bitmap(0,0,0,b, BitmapToByteData.BmpType.Dithering));

                    list.add(DataForSendToPrinterTSC.print(1));
//                    list.add(DataForSendToPrinterTSC.delay(3000+delay));

//                    list.add(DataForSendToPrinterTSC.cut());

                    return list;
                }
            });

        }
    }

    public void printInfo(final Context context) {
            binder.writeDataByYouself(new UiExecute() {
                @Override
                public void onsucess() {
//                    showSnackbar("print succeed",context);

                    PrintPlugin.updatePrintStatus(true,3000);

                }
                @Override
                public void onfailed() {
//                    showSnackbar("print fail",context);

                    PrintPlugin.updatePrintStatus(false,0);
                }
            }, new ProcessData() {
                @Override
                public List<byte[]> processDataBeforeSend() {
                    ArrayList<byte[]> list=new ArrayList<byte[]>();

                    list.add(DataForSendToPrinterTSC.sizeBydot(560,150));
                    list.add(DataForSendToPrinterTSC.cls());
                    list.add(DataForSendToPrinterTSC.direction(1));
                    list.add(DataForSendToPrinterTSC.reference(100,100));
                    list.add(DataForSendToPrinterTSC.speed(7));

                    list.add(DataForSendToPrinterTSC.selfTest());

                    list.add(DataForSendToPrinterTSC.print(1));
                    return list;
                }
            });



    }
    public void printContent(final Context context, final String content) {
        binder.writeDataByYouself(new UiExecute() {
            @Override
            public void onsucess() {
//                showSnackbar("Print succeed", context);
                PrintPlugin.updatePrintStatus(true,1500);
            }

            @Override
            public void onfailed() {
//                showSnackbar("Print fail", context);

                PrintPlugin.updatePrintStatus(false,0);
            }
        }, new ProcessData() {
            @Override
            public List<byte[]> processDataBeforeSend() {
                ArrayList<byte[]> list=new ArrayList<byte[]>();
                list.add(DataForSendToPrinterTSC.codePage("932"));
                list.add(DataForSendToPrinterTSC.sizeBymm(100,30));
                //clear cache
                list.add(DataForSendToPrinterTSC.cls());
                //set the gap
                list.add(DataForSendToPrinterTSC.gapBymm(0,0));
                list.add(DataForSendToPrinterTSC.reference(100,10));
                list.add(DataForSendToPrinterTSC.text(50,50,"KIWI.TTF",0,0,0,"abc,123,テキストの例を印刷する"));

                list.add(DataForSendToPrinterTSC.print(1));

                // list.add(DataForSendToPrinterTSC.cut());

                return list;
            }
        });

    }
    /*
    print barcode
     */
    public void printBarcode(final Context context, final String content){
        binder.writeDataByYouself(new UiExecute() {
            @Override
            public void onsucess() {
//                showSnackbar("Print success", context);

                PrintPlugin.updatePrintStatus(true,1500);
            }

            @Override
            public void onfailed() {
//                showSnackbar("Print fail", context);

                PrintPlugin.updatePrintStatus(false,0);
            }
        }, new ProcessData() {
            @Override
            public List<byte[]> processDataBeforeSend() {
                ArrayList<byte[]> list=new ArrayList<byte[]>();
                //first you have to set the width and heigt ,
                // you can also use dot or inch as a unit method, specific conversion reference programming manual
                list.add(DataForSendToPrinterTSC.sizeBymm(100,30));
                //clear cach
                list.add(DataForSendToPrinterTSC.cls());
                //set the gap
                list.add(DataForSendToPrinterTSC.gapBymm(0,0));
                list.add(DataForSendToPrinterTSC.reference(100,10));
                //print barcode
                list.add(DataForSendToPrinterTSC.barCode(60,50,"128",100,1,0,2,2,content));
//                list.add(DataForSendToPrinterTSC.text(60,155,"3",0,1,0,content));
                //print
                list.add(DataForSendToPrinterTSC.print(1));

                return list;
            }
        });
    }
}
