import 'dart:async';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

class PrintPlugin {
  static const MethodChannel _channel = MethodChannel('print_plugin');

  static void listenerMethod(
    VoidCallback onPrintSuccess, {
    VoidCallback? onPrintFail,
    Function(String)? onBleSuccess,
    VoidCallback? onBleFail,
  }) {
    _channel.setMethodCallHandler((call) async {
      switch (call.method) {
        case 'printSuccess':
          await Future.delayed(Duration(milliseconds: call.arguments));
          log('print success');
          onPrintSuccess();
          break;
        case 'printFail':
          log('print fail');
          onPrintFail!();
          break;

        case 'bleSuccess':
          log('ble success');
          onBleSuccess!(call.arguments);
          break;

        case 'bleFail':
          log('ble fail');
          onBleFail!();
          break;
        default:
      }
    });
  }

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<dynamic> get getDataList async {
    return await _channel.invokeMethod('getListBle');
  }

  static Future<dynamic> printImage(Uint8List data) async {
    return await _channel.invokeMethod('printImage', data);
  }

  static Future<dynamic> printContent(String content) async {
    return await _channel.invokeMethod('printContent', content);
  }

  static Future<dynamic> connectBle(String address, String dis) async {
    return await _channel.invokeMethod('connectBle', [address, dis]);
  }

  static Future<dynamic> printInfo() async {
    return await _channel.invokeMethod('printInfo');
  }

  static Future<dynamic> printBarcode(String content) async {
    return await _channel.invokeMethod('printBarcode', content);
  }
}

Future<File> getImageFileFromAssets(String path) async {
  final byteData = await rootBundle.load('assets/$path');

  final file = File('${(await getTemporaryDirectory()).path}/$path');
  await file.writeAsBytes(byteData.buffer
      .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

  return file;
}
